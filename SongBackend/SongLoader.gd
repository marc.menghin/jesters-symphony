
class_name SongManager

extends Node

const startingSlowingFactor = 1.2
var speed = 200
var slowingFactor  # Adjust the speed as needed

const song_root_path = "res://Songs/"

var songs = []

var json_data

var next_level = 0

var hardMode = false;

var levels
var levelsHard = [
	"res://Songs/FartElise/FartElise.json",
	"res://Songs/A Small Nightfart/a-small-nightfart.json",
	"res://Songs/InTheHallOfFarts/in-the-hall-of-farts.json",
	"res://Songs/FreudeSchönerGötterFart/Joy-beautiful-spark-of-the-gods.json"
]
var levelsEasy = [
	"res://Songs/FartElise/FartElise-easy.json",
	"res://Songs/A Small Nightfart/a-little-nightfart-easy.json",
	"res://Songs/InTheHallOfFarts/in-the-hall-of-farts-easy.json",
	"res://Songs/FreudeSchönerGötterFart/Joy-beautiful-spark-of-the-gods-easy.json"
]

func _ready():
	slowingFactor = startingSlowingFactor
	if(hardMode):
		levels = levelsHard
	else:
		levels = levelsEasy
	load_song(levels[next_level])
	
func set_hard_mode(active):
	if(active):
		levels = levelsHard
	else:
		levels = levelsEasy
		
func set_speed(new_speed):
	speed = new_speed

func get_speed():
	return speed / slowingFactor

func load_level(levelIndex):
	slowingFactor = startingSlowingFactor
	load_song(levels[levelIndex])
	get_tree().change_scene_to_file("res://Level/Main.tscn")

func load_song(song_file):
	var json_text = FileAccess.open(song_file, FileAccess.READ)
	var json_content = json_text.get_as_text()
	# Parse the JSON content into a dictionary
	json_data = JSON.parse_string(json_content)

	# Close the file
	json_text.close()
	
	print("Loaded Song: ", song_file)
	return json_data

#func load_directory(root_directory):
	#print_debug("Searching for Songs in: ", root_directory)
	#var sub_directories = DirAccess.get_directories_at(root_directory)
	#for sub_directory in sub_directories:
		#load_directory(root_directory + "/" + sub_directory)
	#
	#var files_array = DirAccess.get_files_at(root_directory)
	#for song_candidate in files_array:
		#if song_candidate.ends_with(".json"):
			#var song_data = load_song(root_directory + "/" + song_candidate)
			#songs.push_back(song_data)
	
