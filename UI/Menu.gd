extends Control

func _ready():
	$AudioStreamPlayer.play()
	$AnimatedSprite2D.play()

func _on_button_button_down():
	SongLoader.load_level(0)

func _on_button_level_1_button_down():
	SongLoader.load_level(0)

func _on_button_level_2_button_down():
	SongLoader.load_level(1)

func _on_button_level_3_button_down():
	SongLoader.load_level(2)

func _on_button_level_4_button_down():
	SongLoader.load_level(3)

func _on_check_button_toggled(toggled_on):
	print("set hard mode: ",toggled_on)
	SongLoader.set_hard_mode(toggled_on)
