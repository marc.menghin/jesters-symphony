extends Control

var value_label : Label
var posbar : ProgressBar
var negbar : ProgressBar

func _ready():
	posbar = $posBar
	negbar = $negBar

func set_value(new_value):
	if(new_value >=0):
		negbar.value = 0
		posbar.value = new_value
	elif(new_value < 0):
		negbar.value = -new_value
		posbar.value = 0
