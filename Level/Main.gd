extends Node2D

var hits = [0,0,0,0]
var misses = [0,0,0,0]
var longest_combo = 0

var hit_combo = 0
var excitement = 0
var score = 0

var scores



func _ready():
	scores = %ScoreScreen.get_node("FlowContainer/HFlowContainer/Scores")
	var scene = load(SongLoader.json_data.scene)
	var scene_instance = scene.instantiate()
	scene_instance.name = "stage"
	%StageContainer.add_child(scene_instance)
	
	
func _process(_delta):
	if(Input.is_action_just_pressed("menu")):
		get_tree().change_scene_to_file("res://UI/Menu.tscn")
		
	get_node("moodbar").set_value(excitement)

func add_hit(chord):
	print("add hit: ",chord)
	hits[chord] += 1
	hit_combo += 1
	update_excitement()
	add_one_score()
	
func get_hits():
	return hits[0]+hits[1]+hits[2]+hits[3]
	
func add_miss(chord):
	print("add miss: ",chord)
	misses[chord] += 1
	if hit_combo > longest_combo:
		longest_combo = hit_combo
	hit_combo = 0
	update_excitement()
	
func get_misses():
	return misses[0]+misses[1]+misses[2]+misses[3]
	
func update_excitement():
	if hit_combo == 0:
		excitement -= 20
	else:
		excitement += 5 + hit_combo * 1.5
		
	excitement = clampi(excitement, -100, 100)
	
func add_one_score():
	score += excitement + 110
	
func show_score_screen():
	%ScoreScreen.show()
	#hits
	scores.get_node("hits").text = str(get_hits())
	scores.get_node("hits0").text = str(hits[0])
	scores.get_node("hits1").text = str(hits[1])
	scores.get_node("hits2").text = str(hits[2])
	scores.get_node("hits3").text = str(hits[3])
	#misses
	scores.get_node("misses").text = str(get_misses())
	scores.get_node("misses0").text = str(misses[0])
	scores.get_node("misses1").text = str(misses[1])
	scores.get_node("misses2").text = str(misses[2])
	scores.get_node("misses3").text = str(misses[3])
	
	scores.get_node("totalscore").text = str(score)
	remove_crowd()


func _on_button_button_down():
	%ScoreScreen.hide()
	get_tree().change_scene_to_file("res://UI/Menu.tscn")
	
func remove_crowd():
	var crowd_node = get_node("StageContainer/stage/Crowd")
	if crowd_node:
		crowd_node.remove_all_crowd()
	
