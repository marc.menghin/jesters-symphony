extends Node

@export() var crowd: Array[PackedScene] = []

@export_range(-100, 100) var excitement: int = -100
@export var area: CollisionShape2D
@export_range(1,60) var delay: int = 1
@export_range(1, 50) var min_crowd: int = 5
@export_range(1, 50) var max_crowd: int = 30
@export var container: Node;
@export var spawn_point: Node;
@export_range(1, 50) var max_crowd_change: int = 3

var elements: Array[Node]
var currentDelay: float = 0.0
var disable_crowd = false

var mainNode: Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	mainNode = find_parent("Main")
	
	for child in container.get_children():
		container.remove_child(child)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	excitement = mainNode.excitement
	
	currentDelay += delta
	
	if currentDelay >= delay:
		currentDelay -= delay
		update_crowd()
		
	for child in container.get_children():
		child.excitement = excitement

func update_crowd():
	if disable_crowd:
		sfx_none()
		return
		
	var excitement_factor:float = (excitement + 100.0) / 200.0
	var crowd_amount:int = min_crowd + maxi(0, roundi(excitement_factor * (max_crowd - min_crowd)))
	
	var current_amount = container.get_child_count()
	
	if current_amount > crowd_amount:
		var diff = current_amount - crowd_amount
		remove_crowd(mini(diff, max_crowd_change))
	elif current_amount < crowd_amount:
		var diff = crowd_amount - current_amount
		add_crowd(mini(diff, max_crowd_change))
	
	update_sfx();
	
	print_debug("UpdateCrowd >> excitement ", excitement_factor, "crowd amount: ", crowd_amount)
	
func add_crowd(amount:int):
	for i in range(amount):
		var random_crowd = crowd[randi_range(0, crowd.size() - 1)]
		var element:Node = random_crowd.instantiate()
		element.position = spawn_point.position
		element.excitement = excitement
		element.target_position = random_area_position()
		element.source_position = spawn_point.position
		container.add_child(element)
		
func remove_crowd(amount:int):
	for i in range(amount):
		var child = container.get_child(randi_range(0, container.get_child_count() - 1))
		while child.is_leaving and not child.at_position:
			child = container.get_child(randi_range(0, container.get_child_count() - 1))
		child.is_leaving = true
		
func random_area_position():
	var area_rect = area.shape.get_rect()
	return Vector2(area.position.x + randf_range(0,area_rect.size.x), area.position.y + randf_range(0,area_rect.size.y))
	
func remove_all_crowd():
	disable_crowd = true
	for child in container.get_children():
		container.remove_child(child)
		child.queue_free()
	
func update_sfx():
	if excitement > 50:
		sfx_applaus()
	elif excitement < -50:
		sfx_buuh()
	else:
		sfx_none()
	
func sfx_none():
	if %Applaus.playing:
		%Applaus.stop()
	if %Buuh.playing:
		%Buuh.stop()
		
func sfx_applaus():
	if not %Applaus.playing:
		%Applaus.play()
	if %Buuh.playing:
		%Buuh.stop()
		
func sfx_buuh():
	if not %Buuh.playing:
		%Buuh.play()
	if %Applaus.playing:
		%Applaus.stop()
