extends Node2D

@export_range(-100, 100) var excitement: int = 0
@export var movement_speed: float = 0.3
@export var source_position: Vector2
@export var target_position: Vector2
@export var is_leaving: bool = false
@onready var crowd_anim_player: AnimationPlayer = %AnimationPlayer

var time: float = 0.0
var last_excitement = 0
var at_position = false
var random_diviation = 0
var random_delay = 0

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not at_position:
		visible = true
		if position != target_position:
			time += delta * movement_speed
			if time > 1.0:
				time = 0.0
				position = target_position
				at_position = true
			else:
				position = source_position.lerp(target_position, time)
				
	else:
		if random_delay > 0:
			random_delay -= delta
		else:
			if last_excitement != excitement:
				last_excitement = excitement
				update_excitement()
	
	if is_leaving:
		time += delta * movement_speed
		if (time > 1.0):
			visible = false
			get_parent().remove_child(self)
			self.queue_free()
		else:
			position = target_position.lerp(source_position, time)
		
		

func _ready():
	crowd_anim_player.play(&"neutral")
	
func _enter_tree():
	position = source_position
	at_position = false
	time = 0.0
	is_leaving = false
	random_diviation = randi_range(-50, 50)
	random_delay = randf_range(0.0, 2.0)
	
func _exit_tree():
	time = 0.0
	position = source_position
	
func update_excitement():
	var real_excitement = excitement + random_diviation
	if real_excitement < -75:
		crowd_anim_player.play(&"bored")
	elif real_excitement < -50:
		crowd_anim_player.play(&"bored")
	elif real_excitement < -25:
		crowd_anim_player.play(&"neutral")
	elif real_excitement < 0:
		crowd_anim_player.play(&"neutral")
	elif real_excitement < 25:
		crowd_anim_player.play(&"excited1")
	elif real_excitement < 50:
		crowd_anim_player.play(&"excited1")
	elif real_excitement < 75:
		crowd_anim_player.play(&"excited2")
	elif real_excitement <= 100:
		crowd_anim_player.play(&"excited3")
