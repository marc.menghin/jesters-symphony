extends Line2D

var mainref

func _ready():
	mainref = get_node("/root/Main")

func _on_endtrigger_0_area_entered(area):
	var note = area.get_parent().get_parent()
	if(note.visible):
		get_node("endtrigger0/gas0").play()
		mainref.add_miss(0)
	note.queue_free()

func _on_endtrigger_1_area_entered(area):
	var note = area.get_parent().get_parent()
	if(note.visible):
		get_node("endtrigger1/gas1").play()
		mainref.add_miss(1)
	note.queue_free()

func _on_endtrigger_2_area_entered(area):
	var note = area.get_parent().get_parent()
	if(note.visible):
		get_node("endtrigger2/gas2").play()
		mainref.add_miss(2)
	note.queue_free()

func _on_endtrigger_3_area_entered(area):
	var note = area.get_parent().get_parent()
	if(note.visible):
		get_node("endtrigger3/gas3").play()
		mainref.add_miss(3)
	note.queue_free()
