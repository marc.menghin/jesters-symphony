extends Control

# Called every frame
func _process(delta):
	# Move the sprite in the positive X direction
	position.y += SongLoader.get_speed() * delta

# Function to set the height
func set_height(new_height):
	size.y = new_height
	
func get_height():
	return size.y
