extends Node2D

var myNote = preload("res://Level/Soundtrack/note.tscn")
var timeInSong = 0
var waitTime = 0.05
var songdata
var chords
var chordsStack = [null, null, null, null]
var nextFart = [null, null, null, null]
var songTitle
var pause_between_songs = 3


# Stack implementation
class Stack:

	var items = []

	# Custom constructor
	func _init(initial_array: Array):
		items = initial_array.duplicate()
		print("items: ",items)
	# Push an item onto the stack
	func push(item):
		items.append(item)

	# Pop an item from the stack
	func pop():
		if items.size() > 0:
			return items.pop_back()
		else:
			print("Stack is empty.")
			return null

	# Peek at the top item without removing it
	func peek():
		if items.size() > 0:
			return items[items.size() - 1]
		else:
			print("Stack is empty.")
			return null

	# Check if the stack is empty
	func is_empty():
		return items.size() == 0

func _ready():
	songdata = SongLoader.json_data
	chords = songdata.chords
	songTitle = songdata.songname
	$SongTitle.text = songTitle
	$AudioStreamPlayer0.stream = load(songdata.songchord0)
	$AudioStreamPlayer1.stream = load(songdata.songchord1)
	$AudioStreamPlayer2.stream = load(songdata.songchord2)
	$AudioStreamPlayer3.stream = load(songdata.songchord3)
	$AudioStreamPlayerBacking.stream = load(songdata.songdrumms)
	$AudioStreamPlayerBacking2.stream = load(songdata.songmelody)
	print("jsondata in soundtrack",SongLoader.json_data.chords)

func init_song():
	chordsStack[0] = Stack.new(chords[0].active)
	chordsStack[1] = Stack.new(chords[1].active)
	chordsStack[2] = Stack.new(chords[2].active)
	chordsStack[3] = Stack.new(chords[3].active)
	if(!chordsStack[0].is_empty()):
		nextFart[0] = chordsStack[0].pop();
	if(!chordsStack[1].is_empty()):
		nextFart[1] = chordsStack[1].pop();
	if(!chordsStack[2].is_empty()):
		nextFart[2] = chordsStack[2].pop();
	if(!chordsStack[3].is_empty()):
		nextFart[3] = chordsStack[3].pop();

func inst(pos, length):
	var note = myNote.instantiate()
	note.position = pos
	
	# Assuming TextureRect is not a direct child of MainScene
	var vbox = note.get_node("Control/Vbox")
	# Call a function in the other script to set the height
	if vbox:
		#calculate the length of the Vbox
		vbox.set_height(126+(length*4-1)*42)
		#adjust the short farts, they were a bit too early
		if(length == 0.25):
			vbox.position.y = vbox.position.y - 42
		#reposition the long farts 
		vbox.position.y = vbox.position.y - (126+(length*4-1)*42)
		print("length: ",length*4*42)
	else:
		print("vbox not found in the scene.")
		
	var collider = vbox.get_node("Collider/CollisionShape2D")
	# Create a new CapsuleShape2D instance for this scene
	var capsule_shape = CapsuleShape2D.new()
	capsule_shape.set_height(vbox.get_height()+40)
	if(length != 0.25):
		collider.position.y = (vbox.get_height())/2
	collider.shape = capsule_shape;
		
	add_child(note)

func spawnFart(chordIndex, length):
	print("spawn fart at: ",chordIndex)
	if(chordIndex == 0):
		inst(Vector2(55, 10),length)
	elif(chordIndex == 1):
		inst(Vector2(145, 10),length)
	elif(chordIndex == 2):
		inst(Vector2(230, 10),length)
	elif(chordIndex == 3):
		inst(Vector2(315, 10),length)

func start_song():
	print("slowingFactor: ",SongLoader.slowingFactor)
	init_song()
	timeInSong = 0;
	$Timer.start()
	$Button.disabled = true
	$Button.hide()
	wait_and_play(2.8 * SongLoader.slowingFactor)

func check_if_all_empty():
	if(nextFart[0] == null && 
	nextFart[1] == null && 
	nextFart[2] == null && 
	nextFart[3] == null):
		if(SongLoader.slowingFactor > 0.8):
			SongLoader.slowingFactor = SongLoader.slowingFactor-0.1
			await get_tree().create_timer(pause_between_songs).timeout
			start_song()
		else:
			await get_tree().create_timer(pause_between_songs + waitTime).timeout
			get_parent().show_score_screen()
			print("SHOW SCORE SCREEN and then next level!")

func pop_next_fart(chord):
	if(nextFart[chord] != null && nextFart[chord].start*SongLoader.slowingFactor <= timeInSong):
		var length = nextFart[chord].end *SongLoader.slowingFactor - nextFart[chord].start * SongLoader.slowingFactor
		spawnFart(chord,length)
		if(!chordsStack[chord].is_empty()):
			nextFart[chord] = chordsStack[chord].pop()
		else:
			nextFart[chord] = null
			check_if_all_empty()

func _on_timer_timeout():
	pop_next_fart(0)
	pop_next_fart(1)
	pop_next_fart(2)
	pop_next_fart(3)
	timeInSong += waitTime

func _on_button_button_down():
	start_song()

func wait_and_play(wait_time: float) -> void:
	# Pause the function for the specified wait time
	await get_tree().create_timer(wait_time).timeout
	$AudioStreamPlayer0.pitch_scale = (1.0/SongLoader.slowingFactor)
	$AudioStreamPlayer0.play()
	$AudioStreamPlayer1.pitch_scale = (1.0/SongLoader.slowingFactor)
	$AudioStreamPlayer1.play()
	$AudioStreamPlayer2.pitch_scale = (1.0/SongLoader.slowingFactor)
	$AudioStreamPlayer2.play()
	$AudioStreamPlayer3.pitch_scale = (1.0/SongLoader.slowingFactor)
	$AudioStreamPlayer3.play()
	$AudioStreamPlayerBacking.pitch_scale = (1.0/SongLoader.slowingFactor)
	$AudioStreamPlayerBacking.play()
	$AudioStreamPlayerBacking2.pitch_scale = (1.0/SongLoader.slowingFactor)
	$AudioStreamPlayerBacking2.play()
