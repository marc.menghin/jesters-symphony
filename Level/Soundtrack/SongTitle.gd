extends Label

var speed = 0
var started = false

func _ready():
	speed = SongLoader.get_speed()

# Called every frame
func _process(delta):
	if(started):
		# Move the sprite in the positive X direction
		position.y += speed * delta

func _on_button_button_down():
	started = true
