extends Line2D

var chord0_overlapping = null
var chord1_overlapping = null
var chord2_overlapping = null
var chord3_overlapping = null

var AudioStreamPlayer0
var AudioStreamPlayer1
var AudioStreamPlayer2
var AudioStreamPlayer3
var AudioStreamPlayerBacking
var AudioStreamPlayerBacking2
var AudioStreamPlayerFail
var AudioStreamPlayerFail1
var AudioStreamPlayerFail2
var AudioStreamPlayerFail3

var mainref

func _ready():
	mainref = get_node("/root/Main")
	AudioStreamPlayer0 = get_parent().get_node("AudioStreamPlayer0")
	AudioStreamPlayer1 = get_parent().get_node("AudioStreamPlayer1")
	AudioStreamPlayer2 = get_parent().get_node("AudioStreamPlayer2")
	AudioStreamPlayer3 = get_parent().get_node("AudioStreamPlayer3")
	AudioStreamPlayerBacking = get_parent().get_node("AudioStreamPlayerBacking")
	AudioStreamPlayerBacking2 = get_parent().get_node("AudioStreamPlayerBacking2")
	AudioStreamPlayerFail = get_parent().get_node("AudioStreamPlayerFail")
	AudioStreamPlayerFail1 = get_parent().get_node("AudioStreamPlayerFail1")
	AudioStreamPlayerFail2 = get_parent().get_node("AudioStreamPlayerFail2")
	AudioStreamPlayerFail3 = get_parent().get_node("AudioStreamPlayerFail3")

func _process(_delta):
	if(Input.is_action_just_pressed("chord0")):
		get_node("trigger0/hole0").play()
		if(chord0_overlapping != null):
			get_node("trigger0/gas0").play()
			AudioStreamPlayer0.volume_db = 0
			chord0_overlapping.get_parent().get_parent().visible = false
			mainref.add_hit(0)
		else:
			mainref.add_miss(0)
			AudioStreamPlayerFail.play()
	if(Input.is_action_just_pressed("chord1")):
			get_node("trigger1/hole1").play()
			if(chord1_overlapping != null):
				get_node("trigger1/gas1").play()
				AudioStreamPlayer1.volume_db = 0
				chord1_overlapping.get_parent().get_parent().visible = false
				mainref.add_hit(1)
			else:
				mainref.add_miss(1)
				AudioStreamPlayerFail1.play()
	if(Input.is_action_just_pressed("chord2")):
		get_node("trigger2/hole2").play()
		if(chord2_overlapping != null):
			get_node("trigger2/gas2").play()
			AudioStreamPlayer2.volume_db = 0
			chord2_overlapping.get_parent().get_parent().visible = false
			mainref.add_hit(2)
		else:
			mainref.add_miss(2)
			AudioStreamPlayerFail2.play()
	if(Input.is_action_just_pressed("chord3")):
		get_node("trigger3/hole3").play()
		if(chord3_overlapping != null):
			get_node("trigger3/gas3").play()
			AudioStreamPlayer3.volume_db = 0
			chord3_overlapping.get_parent().get_parent().visible = false
			mainref.add_hit(3)
		else:
			mainref.add_miss(3)
			AudioStreamPlayerFail3.play()

func _on_trigger_0_area_entered(area):
	chord0_overlapping = area

func _on_trigger_0_area_exited(_area):
	AudioStreamPlayer0.volume_db = -80
	chord0_overlapping = null

func _on_trigger_1_area_entered(area):
	chord1_overlapping = area

func _on_trigger_1_area_exited(_area):
	AudioStreamPlayer1.volume_db = -80
	chord1_overlapping = null


func _on_trigger_2_area_entered(area):
	chord2_overlapping = area

func _on_trigger_2_area_exited(_area):
	AudioStreamPlayer2.volume_db = -80
	chord2_overlapping = null


func _on_trigger_3_area_entered(area):
	chord3_overlapping = area

func _on_trigger_3_area_exited(_area):
	AudioStreamPlayer3.volume_db = -80
	chord3_overlapping = null
