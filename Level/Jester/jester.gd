extends Node2D

@onready var jester_anim = get_node("JesterAnimation")
var is_farting = false

func _ready():
	jester_anim.play(&"idle")

func _process(_delta):
	if is_farting and jester_anim.animation == &"idle"	:
		jester_anim.play(&"fart")
		
	if is_farting and jester_anim.frame_progress >= 1.0:
		is_farting = false
		jester_anim.play(&"idle")
	


func _input(event):
	if event is InputEventKey and event.pressed:
		is_farting = true
	
